const app = require("express")();
const cors = require("cors");
const bodyParser = require("body-parser");
const http = require('http').Server(app);
const mongoose = require("mongoose");
const io   = require('socket.io')(http);
app.use(bodyParser.json());
app.use(cors());
app.use(
    bodyParser.urlencoded({
        extended: false
    })
)


var Location = require('./models/Location');
//  const mongoURI = 'mongodb+srv://admin:admin@cluster0-4ymgm.mongodb.net/data0'


const mongoURI = 'mongodb://127.0.0.1:27017/data0'

mongoose.set('useCreateIndex', true)
mongoose.connect(mongoURI, {useNewUrlParser: true},)
    .then(()=>console.log("MongoDB connected"))
    .catch(err => console.log(err))

var Users = require('./routes/Users');
app.use('/users', Users);

var Mocs = require('./routes/Mocs');

app.use('/mocs', Mocs)



app.post('/status0', (req,res)=>{

                Location.countDocuments().exec()
                .then(
                    (doc) => {
                new Location({tripid: `V${doc+1}`, date: req.body.date, status: req.body.status}).save()
                res.json({creation: true, tripid_retour: `V${doc+1}`})
                    }).catch(
                        (err) => {
                            console.log(err)
                        }
                    )   
          
})

app.post('/status1', (req,res)=>{
    console.log(req.body)
          
    Location.findOneAndUpdate({
        tripid:req.body.tripid
     }, 
     {
           status:"arret"
    }).exec()
  
})


  io.on('connection', (socket)=>{
    console.log('a user connected');
    socket.on('message', function(data){
        console.log(JSON.stringify(data))
        // console.log(data.data1.locations[0].coords.latitude)
         Location.findOneAndUpdate({
            tripid:data.tripid
         }, 
         {$push : {
                location:{
                        latitude: data.data1.locations[0].coords.latitude,
                        longitude: data.data1.locations[0].coords.longitude,
                        heading: data.data1.locations[0].coords.heading,
                        speed: data.data1.locations[0].coords.speed
                }
            }
        }).exec().then((doc) => {console.log(doc)}).catch((err) => console.log(err))
    
    })
})



const PORT = process.en || 7000;  
http.listen(PORT, ()=>{
    console.log(`Server is running on port ${PORT}`);
});


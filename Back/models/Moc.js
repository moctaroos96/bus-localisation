const mongoose = require("mongoose")


const UserSchema = mongoose.Schema({
    bus:{
        type: String,
        required: true,
    },
    route:{
        type: Array,
        required: true,
    },
    type:{
        type: String,
        required: true,
    },
    v:{
        type: String,
        required: true
    },
});

module.exports = Moc = mongoose.model('mocs', UserSchema)


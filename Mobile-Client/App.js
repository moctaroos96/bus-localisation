import React from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  YellowBox,
} from "react-native";
import MapView, {
  Marker,
  AnimatedRegion,
  PROVIDER_GOOGLE,
  Polyline
} from "react-native-maps";
import haversine from "haversine";
import * as Permissions from 'expo-permissions';
// import SocketIOClient from 'socket.io-client';
import Modal from 'react-native-modal';
import data from './data.json';



const LATITUDE_DELTA = 0.009;
const LONGITUDE_DELTA = 0.009;
const LATITUDE = 14.7645;
const LONGITUDE = -17.366;

YellowBox.ignoreWarnings([
  'Unrecognized WebSocket connection option(s) `agent`, `perMessageDeflate`, `pfx`, `key`, `passphrase`, `cert`, `ca`, `ciphers`, `rejectUnauthorized`. Did you mean to put these under `headers`?'
]);


// const socket = SocketIOClient.connect('http://bus.boki.tech');

// const socket = SocketIOClient.connect('http://192.168.0.115:7000');


class Geo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      latitude: LATITUDE,
      longitude: LONGITUDE,
      routeCoordinates: [],
      prevLatLng: {},
      coordinate: new AnimatedRegion({
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0
      }),
      hasLocationPermissions: false,
      errorMessage: null,
      visibleModal: null,
    };
  }
  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    } else {
      this.setState({ hasLocationPermissions: true });
    }
   }
  /*
  The watchPosition gives us the information about user’s location whenever it get’s changed.
  */
    componentDidMount() {

    this._getLocationAsync();
    // socket.on('connect', ()=>console.log('Connection'));

    const { coordinate } = this.state;

    this.watchID = navigator.geolocation.watchPosition(
      position => {
        // console.log(position)
        // socket.emit('message',position);
        const { routeCoordinates } = this.state;
        const { latitude, longitude } = position.coords;
   
         data.map((data)=>{
          let result = data.route.map((routes)=>{
          //  distance en km
              return  haversine(
              { latitude: position.coords.latitude,longitude:position.coords.longitude },
              { latitude: routes.lat, longitude: routes.lng}
              )
              
          })

          result.sort(function(a, b){return a-b});
          console.log(result)
           
        })
        
        const newCoordinate = {
          latitude,
          longitude
        };
       
          coordinate.timing(newCoordinate).start();
        

        this.setState({
          latitude,
          longitude,
          routeCoordinates: routeCoordinates.concat([newCoordinate]),
          prevLatLng: newCoordinate
        });
      },
      error => console.log(error),
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000,
        distanceFilter: 10
      }
    );
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }



  getMapRegion = () => ({
    latitude: this.state.latitude,
    longitude: this.state.longitude,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA
  });     

  render() {
    if (this.state.errorMessage) {
      alert(this.state.errorMessage);
    } else{
      return (
          <View style={styles.container}>
          <MapView
            style={styles.map}
            provider={PROVIDER_GOOGLE}
            showUserLocation 
            followUserLocation
            loadingEnabled
            region={this.getMapRegion()}
          >
            <Polyline coordinates={this.state.routeCoordinates} />
            <Marker.Animated
              ref={marker => {
                this.marker = marker;
              }}
              coordinate={this.state.coordinate}
            />
          </MapView>
            <View style={styles.buttonContainer}>
                {this._renderButton('Choisir une Bus', () => this.setState({ visibleModal: 1 }))}
                <Modal isVisible={this.state.visibleModal === 1}>
                  {this._renderModalContent()}
                </Modal>
            </View>
        </View>
      );
    }
  }
  _renderButton = (text, onPress) => (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.button}>
        <Text>{text}</Text>
      </View>
    </TouchableOpacity>
  );

  _renderModalContent = () => (
    <View style={styles.modalContent}>
      <Text>Liste des BUS</Text>
      {this._renderButton('Close', () => this.setState({ visibleModal: null }))}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  bubble: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,0.7)",
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20
  },
  latlng: {
    width: 200,
    alignItems: "stretch"
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: "center",
    marginHorizontal: 10
  },
  buttonContainer: {
    flexDirection: "row",
      marginVertical: 10,
    backgroundColor: "transparent",
    padding: 10
  },
  button: {
    backgroundColor: 'white',
    padding: 12,
    margin: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalContent: {
    backgroundColor: 'lightblue',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});

export default Geo;
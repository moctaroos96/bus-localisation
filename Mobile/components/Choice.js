import React from 'react';
import {View, Text, StyleSheet,TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import data from './data.json';
import haversine from "haversine";
import { ScrollView } from 'react-native-gesture-handler';
let result;
let ensemble_bus = new Array();
let latitude0,longitude0
navigator.geolocation.getCurrentPosition(
    (position) => {
        console.log("position", position)
           latitude0= position.coords.latitude,
           longitude0= position.coords.longitude
    },
    (error) => alert(error.message),
    { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
 );
export default class Choice extends React.Component{

    constructor(props){
        super(props);

        this.state={
            visibleModal: null,
            list: []
        }
    }
    onPress = () =>{
            this.setState({ visibleModal: 1 })
            data.map((data)=>{
                result = parseFloat(haversine(
                    p1 = { latitude:14.746403763889008,longitude:-17.370039224624634},
                    p2 = { latitude: data.Stop.lat, longitude: data.Stop.lng}
                    )).toFixed(3) * 1000

                //  console.log(JSON.stringify(data.Stop.lat))
                if(result<500){
                        //  console.log(p2)
                         if(data.Stop.lat==p2.latitude && data.Stop.lng==p2.longitude){
                           ensemble_bus.push(...new Set(data.buses));  
                             /*
                             a = [[1,2,3], [4,5,6], [1,2,3]]
                             b = uniqBy(a, JSON.stringify)
                             console.log(b) // [[1,2,3], [4,5,6]]
                             */
                     }
                }
               
            })
            // if(data.Stop.lat==14.7749369 && data.Stop.lng==-17.3596162){
            //     console.log(data.buses)
            // }
            ensemble_bus.sort();
            console.log([...new Set(ensemble_bus)])
            this.setState({
                list: [...new Set(ensemble_bus)]
            })
    }

    onPress1 = ()=>{
        this.setState({ visibleModal: null })
    }

    render(){
        return(
            <View style={styles.container}>
                   <TouchableOpacity
                        style={styles.button}
                        onPress={this.onPress}
                    >
                        <Text>choisir une ligne</Text>
                    </TouchableOpacity>

                <Modal isVisible={this.state.visibleModal === 1}>
                  <View style={styles.modalContent}>
                      
                    
                            {this.state.list.map(item => (
                                <Text key={item}>{item}</Text>
                                
                                // <TouchableOpacity 
                                // style={styles.button}
                                // onPress={this.onPress1}
                                // key={item}
                                // >
                                //  <Text >{item}</Text> 
                                // </TouchableOpacity>
                           ))}
                    </View>
                 </Modal> 
               
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#34495e"
    },
    button:{
        backgroundColor: 'white',
        padding: 12,
        margin: 16,
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    modalContent:{
        backgroundColor: 'white',
        padding: 22,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    }
})


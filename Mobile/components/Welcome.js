import React from 'react';
import {View,Text,Button,StyleSheet} from 'react-native';

export default class Welcome extends React.Component{
    render(){
        return (
            <View style={styles.container}>
                <Text style={styles.text}>Welcome</Text>
                <Button onPress={()=>{this.props.navigation.navigate("Home")}} title="Se Deconnecter" />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    text:{
        fontSize: 15,
    }
})
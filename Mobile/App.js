import Home from './components/Home';
import Login from './components/Login';
import Welcome from './components/Welcome';
import Register from './components/Register';
import Geo from './components/Geo';
import Choice from './components/Choice';

import {createSwitchNavigator, createAppContainer} from 'react-navigation';

const MainNavigator = createSwitchNavigator({

  Home: {screen: Home},
  Login: {screen: Login},
  Welcome: {screen: Welcome},
  Register: {screen: Register},
  Geo: {screen: Geo},
  Choice: {screen: Choice},

},
{
  initialRouteName: 'Choice'
}


);

const App = createAppContainer(MainNavigator);

export default App;


